<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
class CreateViewsTable extends Migration
{
    public function up()
    {
        Schema::create('views', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_id');
            $table->integer('views');
            $table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists('views');
    }
}
