<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductController;

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware('auth:api')->group(function () {
    Route::resource('products', 'ProductController');
});

// Route::get('/p', function () {
//     return view('profile');
// });
Route::get('/p/{lang}', function ($lang) {
    App::setLocale($lang);
    return view('profile');
});
Route::resource('products', 'ProductController');
Route::post('register',[RegisterController::class,' register']);
Route::get('getAllProducts',[ProductController::class,'getAllProducts']);
Route::get('getAllCategories',[CategoryController::class,'getAllCategories']);
Route::get('search',[ProductController::class,'search']);
Route::get('image/{filename}',[ProductController::class,'image']);
Route::post('login',[RegisterController::class,'login']);
Route::middleware('auth:api')->group(function () {
    Route::resource('products', 'ProductController');
});

Route::post('view',[ViewController::class,'add']);

Route::get('views/{product_id}',[ViewController::class,'getViewsCount']);

Route::middleware('auth:api')->group(function () {
    Route::post('likeProduct',[LikeController::class,'store']);
});
Route::middleware('auth:api')->group(function () {
    Route::post('addComment',[CommentController::class,'store']);
});
Route::get('comments/{product_id}',[CommentController::class,'getProductComments']);