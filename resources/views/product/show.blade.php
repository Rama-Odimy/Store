@extends('product.layout')
@section('content')
<div class="container" style="padding-top: 2%">
    <div class="card">

        <div class="card-body">

            <p class="card-text">product name {{$product->name}}</p>
        </div>
    </div>
</div>
<div class="container" style="padding-top: 2%">
    <div class="form-group">
        <label for="exampleFormControlInput1">Name {{$product->name}}</label>
    </div>
    <div class="form-group">
        <img src="{{asset('images/offers/' . $product->photo)}}" width="100px" height="100px" alt="Image"></div>
    <div class="form-group">
        <label for="exampleFormControlInput1">Price {{$product->price}}</label>
    </div>
    <div class="form-group">
        {!! $product->detail !!}
    </div>

</div>
@endsection