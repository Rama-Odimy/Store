@extends('product.layout')

@section('content')
<div class="jumbotron container">

    <p>It uses utility classes for typography and spacing to space content out within the larger container.</p>

    <a class="btn btn-primary btn-lg" href="#" role="button">Learn more</a>

  </div>

  <div class="container">

    <table class="table">

        <thead class="thead-dark">

          <tr>

            <th scope="col">#</th>

            <th scope="col">name</th>

            <th scope="col">price</th>

            <th scope="col">Actions</th>

          </tr>

        </thead>

        <tbody>

         @foreach ($products as $item)

            <tr>

                <th scope="row">{{++$i}}</th>

                <td>{{$item->name}}</td>

                <td>{{$item->price}}</td>

                <td>

                    <a href="{{ route('products.edit')}}"></a>

                    <a href="{{ route('products.show')}}"></a>

                    <form action="{{ route('products.destroy',$item->id)}}">

                    @csrf

                    @method('DELETE')

                    <BUtton type="submit" class="btn btn-danger">Delete</BUtton>

                    </form>

                </td>

              </tr>

            @endforeach
         
        </tbody>
      </table>
      {!!$products->links()!!}
  </div>
@endsection