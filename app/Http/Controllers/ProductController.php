<?php
namespace App\Http\Controllers;
use App\Product;
use Illuminate\Http\Request;
class ProductController extends Controller
{
    public function getAllProducts()
    {
        //
        $products = Product::all();
        return $this->sendResponse(ProductResource::collection($products), 'all products sent');
    }

    public function trashedProducts()
    {
        // $product = Product::all();
        // $products = Product::withoutTrashed()->get();
        $products = Product::onlyTrashed()->latest()->paginate(2);
        return view('product.trash', compact('products'));
    }
    public function create()
    {
        return view('product.create');
    }
    public function store(Request $request)
    {
        $product = new  Product;
        $product->name = $request->input('name');
        $product->price = $request->input('price');
        $time = $request->input('expr_date');
        $timeStamp = strtotime($time);
        $product->created_date = $timeStamp;
        $product->quantity = $request->input('quantity');
        $product->detail = $request->input('description');
        if ($request->hasfile('photo')) {
            $file = $request->file('photo');
            $extension = $file->getClientOriginalExtension();
            $file_name = time() . '.' . $extension;
            $file->move('images/offers/', $file_name);
            $product->photo = $file_name;
        }
        $product->save();
        return redirect()->route('products.index')->with('success', 'product created successsfully');
    }
    public function show(Product $product)
    {
        return view('product.show', compact('product'));
    }
    public function edit(Product $product)
    {
        return view('product.edit', compact('product'));
    }
    public function update(Request $request, Product $product)
    {
        $request->validate([
            'name' => 'required',
            'price' => 'required',
            'description' => 'required'
        ]);
        // $product = Product::update($request->all());
        $product->update($request->all());
        return redirect()->route('products.index')->with('success', 'product updated successsfully');
    }
    public function destroy(Product $product)
    {
        $product->delete();
        return redirect()->route('products.index')->with('success', 'product deleted successsfully');
    }
    public function softDelete($id)
    {
        $product = Product::find($id)->delete();
        return redirect()->route('products.index')->with('success', 'product deleted successsfully');
    }
    public function backFromSoftDelete($id)
    {
        $product = Product::onlyTrashed()->where('id', $id)->first()->restore();

        return redirect()->route('products.index')->with('success', 'product deleted successsfully');
    }
    public function hardDelete($id)
    {
        $product = Product::onlyTrashed()->where('id', $id)->forceDelete();

        return redirect()->route('product.trash')->with('success', 'product deleted successsfully');
    }
    public function search(Request $request)
    {
        $input = $request->all();
        $search_by = $input['search_by'];
        $search_text = $input['search_text'];
        // $query = Product::where('name', 'bbb')->get();
        $query = Product::where($search_by, 'like', '%' . $search_text . '%')->get();
        return response()->json(['query' => $query]);
    }
}
