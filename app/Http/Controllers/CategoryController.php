<?php
namespace App\Http\Controllers;
use App\Category;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Resources\Category as CategoryResource;
class CategoryController extends BaseController
{
    public function getAllCategories()
    {
        $categories = Category::all();
        return $this->sendResponse(CategoryResource::collection($categories), 'all categories sent');
    }
}


