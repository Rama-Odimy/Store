<?php
namespace App\Http\Controllers;
use App\Like;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
class LikeController extends BaseController
{
    public function store(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'product_id' => 'required',
        ]);
        if ($validator->fails()) {
            return $this->sendError('Validate Error', $validator->errors());
        }
        $like = new  Like;
        $like->product_id = $request->input('product_id');

        $like->user_id = Auth::id();

        $like->save();
        return $this->sendResponse($like, 'like added Successfully!');
    }
}
