<?php
namespace App\Http\Controllers;
use App\Comment;
use App\Http\Resources\Comment as ResourcesComment;
use Illuminate\Http\Request;
use App\View;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
class CommentController extends BaseController
{
    public function store(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'product_id' => 'required',
            'comment_text' => 'required'

        ]);
        if ($validator->fails()) {
            return $this->sendError('Validate Error', $validator->errors());
        }
        $comment = new  Comment;
        $comment->product_id = $request->input('product_id');

        $comment->user_id = Auth::id();
        $comment->comment_text = $request->input('comment_text');
        $comment->save();
        return $this->sendResponse($comment, 'comment added Successfully!');
    }
    public function getProductComments($product_id)
    {
        $comments = Comment::where('product_id', $product_id)->get();
        return $this->sendResponse($comments, 'comments returned successfully!');
    }
}
