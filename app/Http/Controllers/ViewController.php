<?php
namespace App\Http\Controllers;
use App\Http\Controllers\BaseController;
use App\View;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
class ViewController extends BaseController
{
    public function add(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'product_id' => 'required',
        ]);
        if ($validator->fails()) {
            return $this->sendError('Validate Error', $validator->errors());
        }
        $view = new View;
        $view->product_id = $request->input('product_id');
        $id = $request->input('product_id');
        $views = View::where('product_id', $id)->first();
        if (is_null($views)) {
            $view->views = 1;
            $view->save();
            return $this->sendResponse($view, 'view saved Successfully!');
        }
        $view = $views->increment('views');

        return $this->sendResponse($views, 'view updated Successfully!');
    }
    public function getViewsCount($product_id)
    {
        $views = View::where('product_id', $product_id)->first();
        return $this->sendResponse($views, 'view updated Successfully!');
    }
}
