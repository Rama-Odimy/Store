<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class View extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'product_id' => $this->product_id,
            'views' => $this->views,
            'created_at' => $this->created_at->format('d/m/Y')
        ];
    }
}
