<?php
namespace App\Http\Resources;
use Illuminate\Http\Resources\Json\JsonResource;
class Category extends JsonResource
{
    public function toArray($request)
    {
        return [

            'id' => $this->id,
            'cat_name' => $this->cat_name,

        ];
    }
}
