<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class View extends Model
{
    protected $fillable = ['id', 'product_id', 'views'];
    public function product()
    {
        return $this->belongsTo('App\Product', 'product_id');
    }
}

