<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes;
    protected $fillable = ['name', 'photo', 'expr_date', 'category_id','user_id', 'quantity', 'price','description'];
    protected $dates = ['deleted_at'];
//timestamp
    public function setCreatedDateAttribute($value)
    {
        $this->attributes['created_date'] =  Carbon::parse($value);
    }

    public function user()
{
    return $this->belongsTo('App\User', 'user_id');
}
public function category()
{
    return $this->belongsTo('App\Category', 'category_id');
}
}
