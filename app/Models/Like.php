<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Like extends Model
{
    protected $fillable = ['id', 'product_id', 'user_id'];
    public function product()
    {
        return $this->belongsTo('App\Product', 'product_id');
    }
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
